const Enviroment = require('../../../enviroments/Enviroment');

module.exports = MFA_API_URLS = {
    login: `${Enviroment.defineEnviroment().mfaApi.url}/api/login/authenticate`,
    getCountries: `${Enviroment.defineEnviroment().mfaApi.url}/api/countries`,
    getProvinces: `${Enviroment.defineEnviroment().mfaApi.url}/api/countries/id/provinces`,
    getGenders: [Enviroment.defineEnviroment().mfaApi.url, 'api/genders', 'languagCode'],
    getMaritalStatus: [Enviroment.defineEnviroment().mfaApi.url, 'api/marital/status', 'languagCode'],
    getProducts: `${Enviroment.defineEnviroment().mfaApi.url}/api/products`,
    getProductsDetail: [Enviroment.defineEnviroment().mfaApi.url, 'api/products', 'id', 'details/start', 'startDate', 'end', 'endDate'],
    getVoucher: [Enviroment.defineEnviroment().mfaApi.url, 'api/sale/voucher', 'id', 'download'],
    postSale: `${Enviroment.defineEnviroment().mfaApi.url}/api/Sale`
};