class DatabaseRepository
{

    constructor(db) {
        this.db = db;
    }

    async list(collectionName) {
        return await this.db.getCollectionList(collectionName);
    }

    async add(document, collectionName) {
        return await this.db.addDocument(document, collectionName);
    }

    async addWithId(id, document, collectionName) {
        return await this.db.addDocumentWithId(id, document, collectionName);
    }

    async getOne(id, collectionName) {
        return await this.db.getDocumentById(id, collectionName)
    }

    async updateOne(id, collectionName, data) {
        return await this.db.updateDocumentById(id, collectionName, data);
    }

    async deleteOne(id, collectionName) {
        return await this.db.deleteDocumentById(id,collectionName);
    }

    async getCollectionRef(collectionName) {
        return await this.db.getCollection(collectionName);
    }

}

module.exports = DatabaseRepository;