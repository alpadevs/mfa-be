class EmailRepository {
    
    constructor(service) {
        this.service = service;
    }

    initialize() {
        this.service.initialize();
    }

    send(message) {
        this.service.sendEmail(message);
    }
}

module.exports = EmailRepository;