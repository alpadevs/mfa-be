class HttpRepository {
    
    constructor(http) {
        this.http = http;
    }

    get(url, config) {
        return this.http.get(url, config);
    }

    post(url, params, config) {
        return this.http.post(url, params, config);
    }
}

module.exports = HttpRepository;