const EmailRepository = require('./resources/EmailRepositiry');
const DatabaseRepository = require('./resources/databaseRepository');
const HttpRepository = require('./resources/HttpRepository');

const ControllerModule = {
    EmailRepository,
    DatabaseRepository,
    HttpRepository
}

module.exports = ControllerModule;