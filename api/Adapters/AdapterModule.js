const AxiosHttp = require('./resources/Http/Axios');
const Express = require('./resources/Server/Express/Express');
const SendGrid = require('./resources/Email/SendGrid');
const FireStore = require('./resources/Database/FireStore');
const firebaseAdmin = require('./resources/SDK/FirebaseAdmin');

const adapterModule = {
    http: {
        AxiosHttp
    },
    server: {
        Express
    },
    email: {
        SendGrid
    },
    database: {
        FireStore
    },
    sdk: {
        firebaseAdmin
    }
}

module.exports = adapterModule;