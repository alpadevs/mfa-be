const axios = require('axios');

class AxiosHttp {

    constructor() {
        this.http = axios.default;
    }

    get(url, config) {
        return this.http.get(url, config);
    }

    post(url, params, config) {
        return this.http.post(url, params, config)
    }

}

module.exports = AxiosHttp;