const sgMail = require('@sendgrid/mail');
const Enviroment = require('../../../../enviroments/Enviroment');

class SendGrid {

    constructor(){}


    sendEmail(message){  
        sgMail.setApiKey(
            Enviroment.getSendGridId()
        );

        sgMail.send(message, (error) => {
            if(error) {
                console.log(error.toString());
            }else{
                console.log('Message Sended Succesfully');
            }
        });
    }
        

}
module.exports = SendGrid;