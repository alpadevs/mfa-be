const firebaseAdmin = require('firebase-admin');
const Enviroment = require('../../../../enviroments/Enviroment');

firebaseAdmin.initializeApp({
    credential: firebaseAdmin.credential.cert( Enviroment.defineEnviroment().firebase.cert),
    databaseURL: Enviroment.defineEnviroment().firebase.databaseURL
});


module.exports = firebaseAdmin;