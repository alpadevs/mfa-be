const MainRouter = require('./resources/MainRouter');
const EmailRouter = require('./resources/EmailRouter');
const CardsRouter = require('./resources/CardsRouter');
const QuotationsRouter = require('./resources/QuotationsRouter');
const MfaRouter = require('./resources/MyFullAssistRouter');
const PaymentRouter = require("./resources/PaymentRouter");

const RouterModule = {
    MainRouter,
    EmailRouter,
    CardsRouter,
    QuotationsRouter,
    MfaRouter,
    PaymentRouter
}

module.exports = RouterModule;