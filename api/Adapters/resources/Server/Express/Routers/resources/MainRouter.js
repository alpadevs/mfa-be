const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');
class MainRouter {
    constructor() {
        this.router = express.Router()
        this.controller = new controllerModule.MainController();
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.get('',(request, response) => {
            this.controller.root(request,response);
        });          
    }
}

module.exports = MainRouter;