const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');

class EmailRouter {
    
    constructor(emailRepository) {
        this.router = express.Router();
        this.controller = new controllerModule.EmailController(emailRepository);
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.post('', (request, response) => {
            this.controller.sendEmail(request, response);
        });
    }
}

module.exports = EmailRouter;
