const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');

class PaymentRouter {

    constructor(databaseRepository) {
        this.router = express.Router();
        this.controller = new controllerModule.PaymentController(databaseRepository);
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.post('/sign', async (request, response) => {
            await this.controller.generateSignature(request, response);
        });

        this.router.post('/receipt', async (request, response) => {
            this.controller.saveReceiptCybersource(request, response);
        });

        this.router.post('/client/:id', async (request, response) => {
            this.controller.saveClientData(request, response);
        });

        this.router.get('/receipt/:id', async (request, response) => {
            this.controller.getReceipt(request,response);
        });

        this.router.get('/client/:id', async (request, response) => {
            this.controller.getClientData(request,response);
        });
    }

}

module.exports = PaymentRouter;