const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');

class QuotationsRouter {
    constructor(databaseRepository) {
        this.router = express.Router();
        this.controller = new controllerModule.QuotationsController(databaseRepository);
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.get('',(request, response) => {
            this.controller.listQuotations(request,response);
        }); 

        this.router.get('/:id',(request, response) => {
            this.controller.getQuotation(request,response);
        })
        
        this.router.post('', (request, response) => {
            this.controller.addQuotation(request, response);
        })

        this.router.put('/:id', (request, response) => {
            this.controller.updateQuotation(request, response);
        })

        this.router.delete('/:id', (request, response) => {
            this.controller.deleteQuotation(request,response);
        })
    }
}

module.exports = QuotationsRouter;