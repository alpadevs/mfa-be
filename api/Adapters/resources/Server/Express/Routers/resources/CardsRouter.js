const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');

class CardsRouter {
    constructor(databaseRepository) {
        this.router = express.Router();
        this.controller = new controllerModule.CardsController(databaseRepository);
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.get('',(request, response) => {
            this.controller.listCards(request,response);
        }); 

        this.router.get('/:id',(request, response) => {
            this.controller.getCard(request,response);
        })
        
        this.router.post('', (request, response) => {
            this.controller.addCard(request, response);
        })

        this.router.put('/:id', (request, response) => {
            this.controller.updateCard(request, response);
        })

        this.router.delete('/:id', (request, response) => {
            this.controller.deleteCard(request,response);
        })
    }
}

module.exports = CardsRouter;