const controllerModule = require('../../../../../../Controllers/ControllerModule');
const express = require('express');

class MfaRouter {
    
    constructor(httpRepository) {
        this.router = express.Router();
        this.controller = new controllerModule.MyFullAssistController(httpRepository);
        this.defineRoutes();
    }

    defineRoutes() {
        this.router.post('/auth/login', (request, response) => {
            this.controller.login(request, response);
        });

        this.router.get('/countries', (request, response) => {
            this.controller.getCountries(request, response);
        });

        this.router.post('/provinces', (request, response) => {
            this.controller.getProvinces(request, response);
        });

        this.router.post('/genders', (request, response) => {
            this.controller.getGenders(request, response);
        });

        this.router.post('/marital/status', (request, response) => {
            this.controller.getMaritalStatus(request, response);
        });

        this.router.get('/products', (request, response) => {
            this.controller.getProducts(request, response);
        });

        this.router.post('/voucher', (request, response) => {
            this.controller.getVoucher(request, response);
        });

        this.router.post('/products/details', (request, response) => {
            this.controller.getProductsDetails(request, response);
        });

        this.router.post('/sale', (request, response) => {
            this.controller.postSale(request, response);
        });
    }
}

module.exports = MfaRouter;
