const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const RouterModule = require('./Routers/routerModule');

class Express {

    constructor(emailRepository, databaseRepository, httpRepository) {
        this.app = express();
        this.databaseRepository = databaseRepository;
        this.emailRepository = emailRepository;
        this.httpRepository = httpRepository;
    }

    initialize(port) {
        this.configApp();
        this.defineRoutes();
        if (port) {
            this.listenToPort(port);
        }
    }

    configApp() {
        this.configCors();
        this.configBodyParser();
        this.defineRoutes();
        this.configHelment();
    }

    configHelment() {
        this.app.use(helmet());
    }

    configCors() {
        const allowedOrigins = [
            'https://myfullassist.online',
            'https://mfa-web-40e9a.web.app',
            'https://mfa-web-40e9a.firebaseapp.com',
            'https://mfa-web-40e9a--test-j380tpj6.web.app',
            'http://localhost:4200'
        ];
        this.app.use(
            cors({
                origin: function (origin, callback) {

                    if (!origin) return callback(null, true);
                    if (allowedOrigins.indexOf(origin) === -1) {
                        const msg = 'The CORS policy for this site does not ' +
                            'allow access from the specified Origin.';
                        return callback(new Error(msg), false);
                    }
                    return callback(null, true);
                }
            })
        );
    }

    configBodyParser() {
        this.app.use(bodyParser.text({limit: '50mb'}));
        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({ extended: true }));
    }

    defineRoutes() {
        const mainRouter = new RouterModule.MainRouter();
        const emailRouter = new RouterModule.EmailRouter(this.emailRepository);
        const cardRouter = new RouterModule.CardsRouter(this.databaseRepository);
        const quotationRouter = new RouterModule.QuotationsRouter(this.databaseRepository);
        const myFullAssistRouter = new RouterModule.MfaRouter(this.httpRepository);
        const paymentRouter = new RouterModule.PaymentRouter(this.databaseRepository);

        this.app.use('/', mainRouter.router);
        this.app.use('/email', emailRouter.router);
        this.app.use('/card', cardRouter.router);
        this.app.use('/quotation', quotationRouter.router);
        this.app.use('/mfa', myFullAssistRouter.router);
        this.app.use('/payment', paymentRouter.router);
    }

    listenToPort(port) {
        this.app.listen(port, () => {
            console.log("Express is listening to port: " + port);
        })
    }
}

module.exports = Express;
