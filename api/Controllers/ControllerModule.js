
const MainController = require('./resources/MainController');
const EmailController = require('./resources/EmailController');
const CardsController = require('./resources/CardsController');
const QuotationsController = require('./resources/QuotationsController');
const MyFullAssistController = require('./resources/MyFullAssistController');
const PaymentController = require('./resources/PaymentController');

const ControllerModule = {
    MainController,
    EmailController,
    CardsController,
    QuotationsController,
    MyFullAssistController,
    PaymentController
}

module.exports = ControllerModule;