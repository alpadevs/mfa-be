
const Environment = require('../../../enviroments/Enviroment');
const crypto = require('crypto');

class PaymentController {

    constructor(databaseRepository){
        this.databaseRepository = databaseRepository;
        this.collectionName = ['receipt', 'clientData'];
    }

    async generateSignature(request, response) {
        try{
            const signedFields = request.body.signed_field_names.split(',');

            let fieldValues = [];
            signedFields.forEach(item => {
                fieldValues.push(`${item}=${request.body[item]}`);
            });

            const hash = crypto.createHmac('sha256', Environment.defineEnviroment().cyberSource.secret_key)
                .update(fieldValues.join(','))
                .digest('base64');

            const signature = { signature: hash };

            response.send({
                status: 200,
                message: 'Signature generated.',
                payload: signature
            });
        } catch(exception) {
            response.send({
                status: 500,
                message: exception.toString()
            })
        }
    }

    async saveReceiptCybersource(request, response) {
        try{
            let document = request.body;
            console.log(document.req_transaction_uuid);
            let reference = await this.databaseRepository.addWithId(document.req_transaction_uuid, document, this.collectionName[0]);
            response.send({
                status: 200,
                message: 'Data received'
            });
        } catch(exception) {
            response.send({
                status: 500,
                message: exception.toString()
            })
        }
    }

    async saveClientData(request, response) {
        try{
            const document = request.body;
            const id = request.params.id;
            console.log(id);
            let reference = await this.databaseRepository.addWithId(id, document, this.collectionName[1]);
            response.send({
                status: 200,
                message: 'Data saved'
            });
        } catch(exception) {
            response.send({
                status: 500,
                message: exception.toString()
            })
        }
    }

    async getReceipt(request, response) {
        try {
            const id = request.params.id;
            const receipt = await this.databaseRepository.getOne(id, this.collectionName[0]);
            response.send({
                status: '200',
                message: 'this is the receipt with solicitated id',
                payload: receipt
            });   
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async getClientData(request, response) {
        try {
            const id = request.params.id;
            const clientData = await this.databaseRepository.getOne(id, this.collectionName[1]);
            response.send({
                status: '200',
                message: 'this is the client data with solicitated id',
                payload: clientData
            });   
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

}

module.exports = PaymentController;
