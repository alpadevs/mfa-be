class CardsController {
    constructor(databaseRepository){
        this.databaseRepository = databaseRepository;
        this.collectionName = 'cards';
    }
    async listCards(request, response) {
        try{
            let list = await this.databaseRepository.list(this.collectionName);
            response.send({
                status: 200,
                message: 'this is cards list',
                payload: list
            });
        } catch(exception) {
            response.send({
                status: 500,
                message: exception
            })
        }
    }

    async addCard(request, response) {
        try {
            let document = request.body;
            let reference = await this.databaseRepository.add(document, this.collectionName);
            response.send({
                status: '200',
                message: 'added card',
            });
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async getCard(request, response) {
        try {
            const id = request.params.id;
            const card = await this.databaseRepository.getOne(id, this.collectionName);
            response.send({
                status: '200',
                message: 'this is the card with solicitated id',
                payload: card
            });   
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async updateCard(request, response) {
        try {
            const id = request.params.id;
            let newData = request.body;
            const updatedResponse = await this.databaseRepository.updateOne(id, this.collectionName, newData);
            response.send({
                status: '200',
                message: 'card updated',
            }); 
        }  catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async deleteCard(request, response) {
        try {
            const id = request.params.id;
            const deleteResponse = await this.databaseRepository.deleteOne(id, this.collectionName);
            response.send({
                status: '200',
                message: 'card deleted'
            })
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }
}

module.exports = CardsController;
    
