const MFA_API = require('../../Shared/constants/mfa-api-urls.constant');
const MFA_AUTH_CREDENTIALS = require('../../Shared/constants/mfa-auth-credentials.constant');

class MyFullAssistController {
    constructor(httpRepository) {
        this.httpRepository = httpRepository;
        this.mfa_api_url = MFA_API;
        this.mfa_auth_credentials = MFA_AUTH_CREDENTIALS;
    }

    async login(request, response) {
        let userInfo = this.mfa_auth_credentials[request.body.code];

        try {
            let userCode = await this.httpRepository.post(this.mfa_api_url.login, userInfo);

            response.send({
                status: 200,
                payload: userCode.data
            });
        } catch(error){
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getProducts(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        try {
            let products = await this.httpRepository.get(this.mfa_api_url.getProducts, config);

            response.send({
                status: 200,
                payload: products.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getCountries(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        try {
            let countries = await this.httpRepository.get(this.mfa_api_url.getCountries, config);

            response.send({
                status: 200,
                payload: countries.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getProvinces(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        let provincesUrl = this.mfa_api_url.getProvinces;

        let id = request.body.countryId;

        provincesUrl = provincesUrl.replace('id', id);

        try {
            let countries = await this.httpRepository.get(provincesUrl, config);

            response.send({
                status: 200,
                payload: countries.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getGenders(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        let gendersUrl = this.mfa_api_url.getGenders;

        let languageCode = request.body.languageCode;

        gendersUrl[2] = languageCode;
        try {
            let genders = await this.httpRepository.get(new URL(gendersUrl.join('/')).toString(), config);

            response.send({
                status: 200,
                payload: genders.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getMaritalStatus(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        let maritalStatusUrl = this.mfa_api_url.getMaritalStatus;

        let languagCode = request.body.languagCode;

        maritalStatusUrl[2] = languagCode;
        try {
            let maritalStatus = await this.httpRepository.get(new URL(maritalStatusUrl.join('/')).toString(), config);

            response.send({
                status: 200,
                payload: maritalStatus.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }

    async getProductsDetails(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        let productsDetailUrl = this.mfa_api_url.getProductsDetail;

        let id = request.body.id;
        let startDate = new Date(request.body.startDate);
        let endDate = new Date(request.body.endDate);

        productsDetailUrl[2] = id;
        productsDetailUrl[4] = startDate.toDateString();
        productsDetailUrl[6] = endDate.toDateString();

        try {
            let productsDetails = await this.httpRepository.get(new URL(productsDetailUrl.join('/')).toString(), config);

            response.send({
                status: 200,
                payload: productsDetails.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }
    
    async getVoucher(request, response) {
        const config = {
            responseType: 'arraybuffer',
            responseEncoding: 'binary',
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }
        const voucherlUrl = this.mfa_api_url.getVoucher;

        const id = request.body.id;

        voucherlUrl[2] = id;

        try {
            const payload = await this.httpRepository.get(new URL(voucherlUrl.join('/')).toString(), config);

            const textBuffered = Buffer.from(payload.data);
            
            response.send({
                status: 200,
                payload: {
                    content: textBuffered.toString('base64'),
                    filename: "voucher.pdf",
                    type: "application/pdf",
                    disposition: "attachment"
                }
            });
        } catch(error){
            response.status(500)
                    .json({
                        message: error.toString()
                    });
        }
    }

    async postSale(request, response) {
        let config = {
            headers: {
                'Authorization': 'Bearer ' + request.headers.authorization
            }
        }

        let saleInfo = request.body;

        try {
            let sale = await this.httpRepository.post(this.mfa_api_url.postSale, saleInfo, config);

            response.send({
                status: 200,
                payload: sale.data
            });
        } catch (error) {
            response.status(500)
                .json({
                    message: error.toString()
                });
        }
    }
}

module.exports = MyFullAssistController;
    