const Environment = require('../../../enviroments/Enviroment');

class MainController{
    constructor(dataRepository){
        this.dataRepository = dataRepository; 
    }
    
    root(request,response){
        response.send(Environment.defineEnviroment().rootMessage);
    }
}

module.exports = MainController;