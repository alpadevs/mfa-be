class EmailController {

    constructor(emailRepository) {
        this.emailRepository = emailRepository;
    }

    sendEmail(request, response) {
        this.emailRepository.send(request.body);
        try {
            response.send({
                status: '200',
                meessage: 'E-mail succesfully sended',
            });
        } catch (exception) {
            response.send({
                status: '500',
                meessage: 'internal error sending message',
                error: exception.toString()
            })
        }
    }
}

module.exports = EmailController;