class QuotationsController {
    constructor(databaseRepository){
        this.databaseRepository = databaseRepository;
        this.collectionName = 'quotations';
    }
    async listQuotations(request, response) {
        try{
            let list = await this.databaseRepository.list(this.collectionName);
            response.send({
                status: 200,
                message: 'this is quotations list',
                payload: list
            });
        } catch(exception) {
            response.send({
                status: 500,
                message: exception
            })
        }
    }

    async addQuotation(request, response) {
        try {
            let document = request.body;
            let reference = await this.databaseRepository.add(document, this.collectionName);
            response.send({
                status: '200',
                message: 'added quotation',
            });
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async getQuotation(request, response) {
        try {
            const id = request.params.id;
            const quotation = await this.databaseRepository.getOne(id, this.collectionName);
            response.send({
                status: '200',
                message: 'this is the quotation with solicitated id',
                payload: quotation
            });   
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async updateQuotation(request, response) {
        try {
            const id = request.params.id;
            let newData = request.body;
            const updatedResponse = await this.databaseRepository.updateOne(id, this.collectionName, newData);
            response.send({
                status: '200',
                message: 'quotation updated',
            }); 
        }  catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }

    async deleteQuotation(request, response) {
        try {
            const id = request.params.id;
            const deleteResponse = await this.databaseRepository.deleteOne(id, this.collectionName);
            response.send({
                status: '200',
                message: 'quotation deleted'
            })
        } catch(exception) {
            response.send({
                status: '500',
                message: exception
            });
        }
    }
}

module.exports = QuotationsController;
    
