const enviromentDev = require('./resources/enviroment.json');
const enviromentProduction = require('./resources/enviroment.prod.json');
const enviromentConfig = require('./resources/enviroment.config.json');


class Enviroment {
    constructor() {}

    static defineEnviroment() {
        if (enviromentConfig.isProduction) {
            return enviromentProduction;
        } else {
            return enviromentDev;
        }
    }

    static getSendGridId() {
        return this.defineEnviroment().sendGrid.token;
    }
}

module.exports = Enviroment;