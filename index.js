const adapterModule = require('./api/Adapters/AdapterModule');
const repositoriesModule = require('./api/Repositories/RepositoryModule');

const sendGrid = new adapterModule.email.SendGrid();
const emailRepository = new repositoriesModule.EmailRepository(sendGrid);

const fireStore = new adapterModule.database.FireStore();
const databaseRepository = new repositoriesModule.DatabaseRepository(fireStore);

const axiosHttp = new adapterModule.http.AxiosHttp();
const httpRepository = new repositoriesModule.HttpRepository(axiosHttp);

const express = new adapterModule.server.Express(emailRepository, databaseRepository, httpRepository);
express.initialize(8080);
